/**
 * @file
 * JS Implementation of OpenLayers behavior.
 */

/**
 * Geolocate Button Control.  Implements the Geolocate Button OpenLayers
 * Control.
 */
Drupal.openlayers.addBehavior('openlayers_behavior_geolocate_button', function (data, options) {

  // Create new panel control and add.
  var geolocatePanel = new OpenLayers.Control.Panel({
    displayClass: 'openlayers_behavior_geolocate_button_panel'
  });
  data.openlayers.addControl(geolocatePanel);

  // Create new layer to contain location vectors.
  if(options.show_location) {
    var vectorLayer = new OpenLayers.Layer.Vector('geolocateVector');
    data.openlayers.addLayer(vectorLayer);
  }

  // Create toggling control and button.
  var toggler = OpenLayers.Function.bind(
    Drupal.openlayers.geolocateToggle, data, data, options);
  var button = new OpenLayers.Control.Button({
    displayClass: 'openlayers_behavior_geolocate_button',
    title: Drupal.t('Geolocate'),
    trigger: toggler
  });
  geolocatePanel.addControls([button]);
});

/**
 * Toggling function for geolocate control.
 */
Drupal.openlayers.geolocateToggle = function (data, options) {

  // Create Geolocate control
  var geolocate = new OpenLayers.Control.Geolocate(options);
  data.openlayers.addControl(geolocate);

  // Do not watch for changes to the location.
  geolocate.watch = false;

  // Add some event handling
  geolocate.events.register('locationupdated', this, function(e) {
    data.openlayers.setCenter(new OpenLayers.Geometry.Point(e.point.x, e.point.y), options.zoom_level);

    // Handle location showing.
    if(options.show_location) {
      vectorLayer = data.openlayers.getLayersByName('geolocateVector');
      if(typeof vectorLayer[0] != 'object') {
        OpenLayers.Console.log(Drupal.t('geolocateVector layer is not there anymore!'));
        return;
      }
      vectorLayer = vectorLayer[0];
      vectorLayer.removeAllFeatures();

      // Vector to illustrate accuracy.
      var accuracyCircle = new OpenLayers.Feature.Vector(
        OpenLayers.Geometry.Polygon.createRegularPolygon(
          new OpenLayers.Geometry.Point(e.point.x, e.point.y),
          e.position.coords.accuracy/2,
          40,
          0
        ),
        {},
        options.show_location_accuracy_styles
      );

      // Vector to illustrate position.
      var positionVector = new OpenLayers.Feature.Vector(
        e.point,
        {},
        options.show_location_position_styles
      );

      // Add vectors.
      vectorLayer.addFeatures([
        accuracyCircle,
        positionVector
      ]);
    }
  });
  geolocate.events.register('locationfailed', this, function(e) {
    OpenLayers.Console.log(Drupal.t('Location detection failed'));
  });

  // Activate!
  geolocate.activate();
};