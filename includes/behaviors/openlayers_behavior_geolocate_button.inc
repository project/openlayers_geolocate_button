<?php
/**
 * @file
 * Implementation of OpenLayers behavior.
 */

/**
 * Geolocate Button Behavior
 * http://dev.openlayers.org/docs/files/OpenLayers/Control/Geolocate-js.html
 */
class openlayers_behavior_geolocate_button extends openlayers_behavior_geolocate {
  /**
   * Provide initial values for options.
   */
  function options_init() {
    return array(
      'bind' => TRUE,
      'geolocationOptions' => array(
        'enableHighAccuracy' => FALSE,
        'maximumAge' => 0,
        'timeout' => 7000,
      ),
      'zoom_level' => 12,
      'show_location' => FALSE,
      'show_location_accuracy_styles' => "fillColor|#000\nfillOpacity|0.25\nstrokeWidth|0",
      'show_location_position_styles' => "fillOpacity|0\ngraphicName|cross\npointRadius|10\nstrokeColor|#f00\nstrokeWidth|2",
    );
  }

  function options_form($defaults = array()) {
    $intials = $this->options_init();

    return array(
      'bind' => array(
        '#title' => t('Center when located'),
        '#type' => 'select',
        '#options' => array(
          TRUE => t('Yes'),
          FALSE => t('No'),
        ),
        '#description' => t('When enabled, if the geolocation control finds a location, it will set the center of the map at this point.'),
        '#default_value' => isset($defaults['bind']) ? $defaults['bind'] : $intials['bind'],
      ),
      'zoom_level' => array(
        '#title' => t('Zoom level'),
        '#type' => 'textfield',
        '#description' => t('An integer zoom level for when a location is found.  0 is the most zoomed out and higher numbers mean more zoomed in (the number of zoom levels depends on your map).'),
        '#default_value' => isset($defaults['zoom_level']) ? $defaults['zoom_level'] : $intials['zoom_level'],
      ),
      'show_location' => array(
        '#title' => t('Show location on map'),
        '#type' => 'select',
        '#options' => array(
          TRUE => t('Yes'),
          FALSE => t('No'),
        ),
        '#description' => t('When enabled, if the geolocation control finds a location, it will show indicator on that location.'),
        '#default_value' => isset($defaults['show_location']) ? $defaults['show_location'] : $intials['show_location'],
      ),
      'show_location_accuracy_styles' => array(
        '#title' => t('Show location accuracy circle styles'),
        '#type' => 'textarea',
        '#description' => t('When Show location is enabled, define styles for accuracy vector. Enter one style property per line, in the format key|value. Options: http://dev.openlayers.org/apidocs/files/OpenLayers/Feature/Vector-js.html#OpenLayers.Feature.Vector.OpenLayers.Feature.Vector.style'),
        '#default_value' => isset($defaults['show_location_accuracy_styles']) ? $defaults['show_location_accuracy_styles'] : $intials['show_location_accuracy_styles'],
      ),
      'show_location_position_styles' => array(
        '#title' => t('Show location position styles'),
        '#type' => 'textarea',
        '#description' => t('When Show location is enabled, define styles for position vector. Enter one style property per line, in the format key|value. Options: http://dev.openlayers.org/apidocs/files/OpenLayers/Feature/Vector-js.html#OpenLayers.Feature.Vector.OpenLayers.Feature.Vector.style'),
        '#default_value' => isset($defaults['show_location_position_styles']) ? $defaults['show_location_position_styles'] : $intials['show_location_position_styles'],
      ),
    );
  }

  function js_dependency() {
    return array(
      'OpenLayers.Control.Button',
      'OpenLayers.Control.Geolocate',
    );
  }

  /**
   * Render.
   */
  function render(&$map) {
    //make sure that $option['bind'] is a true boolean, and not a string '0'.
    $this->options['bind'] = !!$this->options['bind'];

    // Make sure that $option['show_location'] is a true boolean, and not a string '0'.
    $this->options['show_location'] = !!$this->options['show_location'];

    // Handle style fields with key val data.
    $style_fields = array('show_location_accuracy_styles', 'show_location_position_styles');
    foreach($style_fields as $field) {
      // Taken from list field handling.
      $values = array();
      $list = explode("\n", $this->options[$field]);
      $list = array_map('trim', $list);
      $list = array_filter($list, 'strlen');
      foreach ($list as $position => $text) {
        $value = $key = FALSE;
        if (preg_match('/(.*)\|(.*)/', $text, $matches)) {
          $values[$matches[1]] = $matches[2];
        }
      }
      $this->options[$field] = $values;
    }

    drupal_add_css(drupal_get_path('module', 'openlayers_geolocate_button') .
      '/includes/behaviors/openlayers_behavior_geolocate_button.css');
    drupal_add_js(drupal_get_path('module', 'openlayers_geolocate_button') .
      '/includes/behaviors/openlayers_behavior_geolocate_button.js');
    return $this->options;
  }
}
